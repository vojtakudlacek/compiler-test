mod tokenizer;
use std::env;
use std::fs;

fn main() {
    let contents = fs::read_to_string("test.ct")
        .expect("Something went wrong reading the file");
    let tokens = tokenizer::tokenize(&contents);
    println!("{:?}", tokens);
}
