#[derive(Debug)]
enum TokenType {
    SEMICOLON,
    PLUS,
    MINUS,
    STAR,
    SLASH,

    RETURN,
    EXIT,

    IDENTIFIER,
    STRING_LITERAL,
    INT_LITERAL,
}

#[derive(Debug)]
pub struct Token {
    token_type: TokenType,
    value: Option<String>,
}

pub fn tokenize(input: &str) -> Vec<Token> {
    let mut tokens = Vec::new();
    let mut chars = input.chars().peekable();

    'outer: loop {
        if chars.peek().unwrap().is_alphabetic() {
            //Keyword or identifier tokenization

            let mut value = String::new();
            'inner: loop {
                value.push(*chars.peek().unwrap());
                chars.next();
                if chars.peek().is_none() {
                    break 'inner;
                } else if !chars.peek().unwrap().is_alphanumeric() {
                    break 'inner;
                }
            }

            match value.as_str() {
                "return" => tokens.push(Token {
                    token_type: TokenType::RETURN,
                    value: None,
                }),
                "exit" => tokens.push(Token {
                    token_type: TokenType::EXIT,
                    value: None,
                }),
                _ => tokens.push(Token {
                    token_type: TokenType::IDENTIFIER,
                    value: Some(value),
                }),
            }

            if chars.peek().is_none() {
                break 'outer;
            }
            continue 'outer;

        } else if chars.peek().unwrap().is_digit(10) {
            //Integer literal tokenization

            let mut value = String::new();
            'inner: loop {
                value.push(*chars.peek().unwrap());
                chars.next();
                if chars.peek().is_none() {
                    break 'inner;
                } else if !chars.peek().unwrap().is_digit(10) {
                    break 'inner;
                }
            } 

            tokens.push(Token {
                token_type: TokenType::INT_LITERAL,
                value: Some(value),
            });

            if chars.peek().is_none() {
                break 'outer;
            }
            continue 'outer;

        } else if *chars.peek().unwrap() == '\'' || *chars.peek().unwrap() == '\"' {
            //String litreal tokenization
            
            let mut value = String::new();
            let quote = *chars.peek().unwrap();
            chars.next();

            'inner: loop {
                if chars.peek().is_none() {
                    panic!("Unexpected end of input");
                } else if *chars.peek().unwrap() == '\\' {
                    //Escape sequence

                    chars.next();
                    if chars.peek().is_none() {
                        panic!("Unexpected end of input");
                    } else if *chars.peek().unwrap() == quote {
                        value.push(*chars.peek().unwrap());
                        chars.next();
                    } else if *chars.peek().unwrap() == '\\' {
                        value.push('\\');
                        chars.next();
                    } else if *chars.peek().unwrap() == 'n' {
                        value.push('\n');
                        chars.next();
                    } else if *chars.peek().unwrap() == 't' {
                        value.push('\t');
                        chars.next();
                    } else if *chars.peek().unwrap() == 'r' {
                        value.push('\r');
                        chars.next();
                    } else {
                        panic!("Unexpected character: {}", chars.peek().unwrap());
                    }
                } else if *chars.peek().unwrap() == quote {
                    chars.next();
                    break 'inner;
                } else {
                    value.push(*chars.peek().unwrap());
                    chars.next();
                }
            }

            tokens.push(Token {
                token_type: TokenType::STRING_LITERAL,
                value: Some(value),
            });

            if chars.peek().is_none() {
                break 'outer;
            }
            continue 'outer;

        } else {
            //Symbol tokenization

            match *chars.peek().unwrap() {
                ' ' => {},
                '\n' => {},
                '\t' => {},
                ';' => tokens.push(Token {
                    token_type: TokenType::SEMICOLON,
                    value: None,
                }),
                '+' => tokens.push(Token {
                    token_type: TokenType::PLUS,
                    value: None,
                }),
                '-' => tokens.push(Token {
                    token_type: TokenType::MINUS,
                    value: None,
                }),
                '*' => tokens.push(Token {
                    token_type: TokenType::STAR,
                    value: None,
                }),
                '/' => tokens.push(Token {
                    token_type: TokenType::SLASH,
                    value: None,
                }),
                _ => panic!("Unexpected character: {}", chars.peek().unwrap()),
            }
        }

        chars.next();
        if chars.peek().is_none() {
            break 'outer;
        }
    } 

    return tokens;
}
